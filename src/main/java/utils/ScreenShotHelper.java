package utils;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
public class ScreenShotHelper {
	
	String FileName;
	TakesScreenshot takesScreenshot ;
	 public WebDriver browser;
	

	public ScreenShotHelper(WebDriver driver) {
		super();
		
		this.browser=driver;
	}
	
	public String doScreenShot() {
		takesScreenshot = (TakesScreenshot) browser;
		 File scrFile = takesScreenshot.getScreenshotAs(OutputType.FILE);
	        Date date = new Date();
	        File destFile = getDestinationFile(String.valueOf(date.getTime()));
	        try {
	        	
	            FileUtils.copyFile(scrFile, destFile);
	            return destFile.toString();

	        } catch (IOException ioe) {
	            throw new RuntimeException(ioe);
	            
	        }	
	}
	
	private File getDestinationFile(String message) {
        String userDirectory = System.getProperty("user.dir") +"/test-output/";
        String fileName = message+".jpg";
        String absoluteFileName = userDirectory + "/" + fileName;
        return new File(absoluteFileName);
    }
	
	

}
