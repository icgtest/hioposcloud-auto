package utils;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.relevantcodes.extentreports.ExtentReports;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;

public class Variables {
	
	 private static String reportDirectory = "reports";
	 public static ScreenShotHelper shelper;
	 private static String reportFormat = "xml";
	
	public static String loginName="autohpcv2";
	public static String loginPass="terminal1";
	public static ExtentReports extent;
	
	public static String cordBotonSustituir="";
	public static String HioPosCloudPackage="icg.android.start";
	public static String HioPosCloudStartActivity=".StartActivity";
	public static String PathApkHioPosCloud="C:\\EclipseProjects\\HioPos Cloud_2.42.0.1.apk";
	//public static String PathApkHioPosCloud="C:\\EclipseProjects\\HioPosCloud_old.apk";
	
	public static DesiredCapabilities dc = null;
	public static String URL = "http://localhost:4730/wd/hub";
	public static AndroidDriver<AndroidElement> driver = null;
	
	public static void configureReport() {
    	Variables.extent = new ExtentReports (System.getProperty("user.dir") +"/test-output/STMExtentReport.html", true);
    	Variables.extent
        .addSystemInfo("Host Name", "Departamento de Calidad")
        .addSystemInfo("Environment", "Automation Testing")
        .addSystemInfo("User Name", "Oscar M");
    	Variables.extent.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
    } 
	
	public static void deleteReports() throws IOException {
		FileUtils.cleanDirectory(new File("C:\\EclipseProjects\\HPCStartAndLogin\\test-output"));
	}
	
	
	
	public static DesiredCapabilities setDesiredCababilities(String testName,Boolean installApk, Boolean openApk) {
			dc = new DesiredCapabilities();
		    dc.setCapability("reportDirectory", reportDirectory);
	        dc.setCapability("reportFormat", reportFormat);
	        dc.setCapability("testName", testName);
	        dc.setCapability(MobileCapabilityType.UDID, Variables.Devices.POS);
	        if (installApk)dc.setCapability(MobileCapabilityType.APP, Variables.PathApkHioPosCloud); //instala la app
	        if (openApk)dc.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, Variables.HioPosCloudPackage);
	        if (openApk)dc.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, Variables.HioPosCloudStartActivity);
	        return dc;
	}
	
	public DesiredCapabilities getDesiredCapabilities(String testName,Boolean installApk, Boolean openApk) {
		if (dc!=null) {
			return dc;
		}
		
		
		
		return setDesiredCababilities(testName,installApk,openApk);
	}
	
	public class Devices{
		
    	 public static final String BQ="TF048589";
    	 public static final String POS="CO6U42YTTK";
			
	}
	
	public class Seller{
		public static final String name="Seller1";
		public static final int positionX=173;
		public static final int positionY=305;
	}
	
	public class ViewsTestLogin{
		//Login
		public static final String pantallaIdiomas="(//*[@class='android.widget.LinearLayout' and ./parent::*[@class='android.widget.ScrollView']]/*[@class='android.view.View'])[1]";
		public static final String botonEspanol="(//*[@class='android.widget.LinearLayout' and ./parent::*[@class='android.widget.ScrollView']]/*[@class='android.view.View'])[1]";
		public static final String pantallaLogin="//*[@text='Identificaci�n']";
		public static final String campoNombreLogin="//*[@class='android.widget.EditText'][1]";
		public static final String campoPasswordLogin="//*[@class='android.widget.EditText'][2]";
		public static final String campoEmailConfiguracionTienda="//*[@text='Email']";
		public static final String labelIdentificacion="//*[@text='Identificaci�n']";
		public static final String campoNumeroTienda="//*[@class='android.widget.TextView'][4]";
		public static final String campoEmailTienda="//*[@class='android.widget.TextView'][3]";
		//Hasta aqu� el Login	
	}
	
	public class ViewsConfiguracion{
		public static final String campoNombreConfiguracion="//*[@class='android.widget.EditText' and (./preceding-sibling::* | ./following-sibling::*)[@text='Nombre']]";
		public static final String campoNombreFiscalConfiguracion="//*[@class='android.widget.EditText' and (./preceding-sibling::* | ./following-sibling::*)[@text='Nombre fiscal']]";
		public static final String campoNIFConfiguracion="//*[@class='android.widget.EditText' and (./preceding-sibling::* | ./following-sibling::*)[@text='NIF']]";
		public static final String campoDireccionConfiguracion="//*[@class='android.widget.EditText' and (./preceding-sibling::* | ./following-sibling::*)[@text='Direcci�n']]";
		public static final String campoCPConfiguracion="//*[@class='android.widget.EditText' and (./preceding-sibling::* | ./following-sibling::*)[@text='C�digo postal']]";
		public static final String campoPoblacionConfiguracion="//*[@class='android.widget.EditText' and (./preceding-sibling::* | ./following-sibling::*)[@text='Poblaci�n']]";
		public static final String campoProvinciaConfiguracion="//*[@class='android.widget.EditText' and (./preceding-sibling::* | ./following-sibling::*)[@text='Provincia']]";
		public static final String campoTlfConfiguracion="//*[@class='android.widget.EditText' and (./preceding-sibling::* | ./following-sibling::*)[@text='Tel�fono']]";
		public static final String campoEmailConfiguracion="//*[@class='android.widget.EditText' and (./preceding-sibling::* | ./following-sibling::*)[@text='Email']]";
		public static final String pantallaDispositivos="(//*[@class='android.widget.LinearLayout' and ./parent::*[@class='android.widget.ScrollView']]/*[@class='android.view.View'])[4]";
		public static final String campoModeloImpresora="(//*[@class='android.widget.LinearLayout' and ./parent::*[@class='android.widget.ScrollView' and (./preceding-sibling::* | ./following-sibling::*)[@class='android.view.View']]]/*[@class='android.view.View'])[1]";
		public static final String campoConexionImpresora="(//*[@class='android.widget.LinearLayout' and ./parent::*[@class='android.widget.ScrollView' and (./preceding-sibling::* | ./following-sibling::*)[@class='android.view.View']]]/*[@class='android.view.View'])[1]";
		
	}
	
	public class configuracionOptions{
		public class vendedoresButton{
			public static final int posicionX=124;
			public static final int posicionY=484;
		}
		public class nuevoVendedorButton{
			public static final int posicionX=200;
			public static final int posicionY=884;
		}
	}
	
	public class topBarButtons{
		public class AdministracionButton{
			public static final int posicionX=1045;
			public static final int posicionY=44;
			
			public class ConfiguracionButton{
				public static final int posicionX=1020;
				public static final int posicionY=116;
				
			}
		}
		
		public class CerrarButton{
			public static final int posicionX=1861;
			public static final int posicionY=34;
		}
	}
	
	public class tecladoNumerico{
		
		public class Punto{
			public static final int posicionX=1763;
			public static final int posicionY=983;
		}
		
		public class Cero{
			public static final int posicionX=1608;
			public static final int posicionY=983;
		}
		
		public class Uno{
			public static final int posicionX=1517;
			public static final int posicionY=884;
		}
		
		public class Dos{
			public static final int posicionX=1640;
			public static final int posicionY=884;
		}
		
		public class Tres{
			public static final int posicionX=1757;
			public static final int posicionY=884;
		}
		
		public class Cuatro{
			public static final int posicionX=1536;
			public static final int posicionY=783;
		}
		
		public class Cinco{
			public static final int posicionX=1640;
			public static final int posicionY=783;
		}
		
		public class Seis{
			public static final int posicionX=1746;
			public static final int posicionY=783;
		}
		
		public class Siete{
			public static final int posicionX=1533;
			public static final int posicionY=672;
		}
		
		public class Ocho{
			public static final int posicionX=1649;
			public static final int posicionY=672;
		}
		
		public class Nueve{
			public static final int posicionX=1752;
			public static final int posicionY=672;
		}
		
		public class Borrar{
			public static final int posicionX=1853;
			public static final int posicionY=672;
		}
		
		public class Menos{
			public static final int posicionX=1872;
			public static final int posicionY=778;
		}
		
		public class Por{
			public static final int posicionX=1867;
			public static final int posicionY=881;
		}
		
		public class Intro{
			public static final int posicionX=1867;
			public static final int posicionY=991;
		}
	}
	

}