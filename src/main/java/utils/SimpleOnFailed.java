package utils;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import org.apache.commons.io.FileUtils;
import org.junit.AssumptionViolatedException;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class SimpleOnFailed extends TestWatcher {
	 ExtentTest logger;
	 ExtentReports report;
	 TakesScreenshot takesScreenshot ;
	  public WebDriver browser;
	  public Boolean strictMode=false;
	  
	public SimpleOnFailed(WebDriver browser,ExtentTest logger,ExtentReports report,Boolean strictMode) {
		super();
		this.browser = browser;
		this.report=report;
		this.strictMode=strictMode;
	}


	public ExtentTest getLogger() {
		return logger;
	}
	
	
	


	public Boolean getStrictMode() {
		return strictMode;
	}


	public void setStrictMode(Boolean strictMode) {
		this.strictMode = strictMode;
	}


	public ExtentReports getReport() {
		return report;
	}


	public void setReport(ExtentReports report) {
		this.report = report;
	}


	public void setLogger(ExtentTest logger) {
		this.logger = logger;
	}


	public SimpleOnFailed(ExtentTest logger) {
		super();
		this.logger = logger;
	}
	
	
	@Override
	protected void finished(Description description) {
		// TODO Auto-generated method stub
		super.finished(description);
	}


	@Override
	protected void skipped(AssumptionViolatedException e, Description description) {
		// TODO Auto-generated method stub
		super.skipped(e, description);
	}


	@Override
	protected void starting(Description description) {
		// TODO Auto-generated method stub
		super.starting(description);
	}

	@Override
	protected void succeeded(Description description) {
		// TODO Auto-generated method stub
		super.succeeded(description);
	}

	@Override
    protected void failed(Throwable e, Description description) {
        System.out.println("Only executed when a test fails "+e.getMessage());  
        takesScreenshot = (TakesScreenshot) browser;

        File scrFile = takesScreenshot.getScreenshotAs(OutputType.FILE);
        Date date = new Date();
        File destFile = getDestinationFile(String.valueOf(date.getTime()));
        try {
        	
            FileUtils.copyFile(scrFile, destFile);
            logger.log(LogStatus.FAIL, e.getMessage() + "<a target='_blank' href='"+destFile+"'> (ScreenShot)</a>");
            report.endTest(logger);
            report.flush();
            if (strictMode) {System.exit(0);}
  
        } catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }
    }
	
	private File getDestinationFile(String message) {
        String userDirectory = System.getProperty("user.dir") +"/test-output/";
        String fileName = message+".jpg";
        String absoluteFileName = userDirectory + "/" + fileName;
        return new File(absoluteFileName);
    }
	


}