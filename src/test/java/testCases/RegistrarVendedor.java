package testCases;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.LogStatus;


import utils.Variables;

public class RegistrarVendedor extends testHelper {
	
	
	
    @Test(timeout=30000)   
	public void registrarSeller1() {
  	     logger = Variables.extent.startTest("Registrar Vendedor");
		 ruleExample.setLogger(logger);
		 ruleExample.browser=Variables.driver;
		 ruleExample.setReport(Variables.extent);
		 
		 new WebDriverWait(Variables.driver, 120).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='sellerPager']")));
		 sleep(500);
		 Variables.driver.tap(1,Variables.Seller.positionX,Variables.Seller.positionY,100); //pulsamos en Seller 1
         Boolean isPresent = Variables.driver.findElements(By.xpath("//*[@id='roomSurface']")).size() > 0; // verifico si he llegado a la pantalla de mesas
         Assert.assertTrue(isPresent);
         logger.log(LogStatus.INFO, "Vendedor registrado - HioPosCloud en pantalla de mesas");
         logger.log(LogStatus.PASS, "Test 'Registrar Vendedor' se ha completado con exito <a target='_blank' href='"+Variables.shelper.doScreenShot()+"'>ScreenShot</a>");

	}
	
	

}
