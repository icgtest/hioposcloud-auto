/**
 * 
 */
package testCases;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.LogStatus;
import utils.Variables;




public class Clientes extends testHelper{
	
    @Test(timeout=1200000)   
	public void CrearCliente() {
    	 //SQLHelper.executeSQL("select Name from DB28445.Product");
    
  	     logger = Variables.extent.startTest("Crear Clientes - Validar Clientes - Borrar Clientes"); 
		 ruleExample.setLogger(logger);
		 ruleExample.browser=Variables.driver;
		 ruleExample.setReport(Variables.extent); 
		 loginVendedorSeller1();
		 abrirClientes();
		 

		 logger.log(LogStatus.PASS, "Crear Clientes - Validar Clientes - Borrar Clientes se ha completado con exito. <a target='_blank' href='"+Variables.shelper.doScreenShot()+"'>ScreenShot</a>");
 

    }

	private void abrirClientes() {
		new WebDriverWait(Variables.driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='roomSurface']")));//pantalla de mesas
		 tap(Variables.topBarButtons.AdministracionButton.posicionX,Variables.topBarButtons.AdministracionButton.posicionY);//administración
		 tap(Variables.topBarButtons.AdministracionButton.ConfiguracionButton.posicionX,Variables.topBarButtons.AdministracionButton.ConfiguracionButton.posicionY);//configuración
		 new WebDriverWait(Variables.driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@text='Configuración']"))); // esperamos hasta la pantalla de configuración
		 
		
	}

	private void loginVendedorSeller1() {
		tap(Variables.Seller.positionX,Variables.Seller.positionY);
		
	}
	 	 

}