package testCases;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Rule;

import com.relevantcodes.extentreports.ExtentTest;

import io.appium.java_client.android.AndroidDriver;
import utils.ScreenShotHelper;
import utils.SimpleOnFailed;
import utils.Variables;

public class testHelper {
	
	 static {
	      System.setProperty("org.apache.commons.logging.Log",
	                         "org.apache.commons.logging.impl.NoOpLog");
	   }
	
	@BeforeClass
    public static void setUp() throws MalformedURLException {
		
		Set<String> artifactoryLoggers = new HashSet<>(Arrays.asList("org.apache.http", "groovyx.net.http"));
		for(String log:artifactoryLoggers) {
		    ch.qos.logback.classic.Logger artLogger = (ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory.getLogger(log);
		    artLogger.setLevel(ch.qos.logback.classic.Level.INFO);
		    artLogger.setAdditive(false);
		}
      
    	if (Variables.dc==null) { 
    	 	 Variables.dc = Variables.setDesiredCababilities("HPC AUTOMATION",false,false);
             Variables.driver = new AndroidDriver<>(new URL(Variables.URL),Variables.dc);
             Variables.driver.setLogLevel(Level.INFO);
     		 Variables.configureReport();	
     		 Variables.shelper = new ScreenShotHelper(Variables.driver);
    	}
	
    }
	
	static ExtentTest logger;
	@Rule
    public  SimpleOnFailed ruleExample = new SimpleOnFailed(Variables.driver,logger,Variables.extent,false);
	
	
	 public static void sleep(int mili) {
    	 try{Thread.sleep(mili);} catch(Exception ignore){}
    }
	 
	 public static void tap(int x, int y) {
		 sleep(500);
 		 Variables.driver.tap(1,x,y,100);//tap
 		 sleep(500);

    }
	 
	 @After 
	    public void afterEachTest() {
	       Variables.extent.endTest(logger);
	       Variables.extent.flush();
	    }

	 
}


