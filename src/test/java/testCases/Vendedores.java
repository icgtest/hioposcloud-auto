/**
 * 
 */
package testCases;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.relevantcodes.extentreports.LogStatus;

import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;
import utils.Variables;
import utils.ImageResizer;
import utils.SQLHelper;


public class Vendedores extends testHelper{
	
    @Test(timeout=1200000)   
	public void Test1_Vendedores() {
    	 //SQLHelper.executeSQL("select Name from DB28445.Product");
    
  	     logger = Variables.extent.startTest("Crear Vendedor - Login Vendedor - Borrar Vendedor"); 
		 ruleExample.setLogger(logger);
		 ruleExample.browser=Variables.driver;
		 ruleExample.setReport(Variables.extent); 
 		 new WebDriverWait(Variables.driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='roomSurface']")));//pantalla de mesas
 		 tap(Variables.topBarButtons.AdministracionButton.posicionX,Variables.topBarButtons.AdministracionButton.posicionY);//administración
 		 tap(Variables.topBarButtons.AdministracionButton.ConfiguracionButton.posicionX,Variables.topBarButtons.AdministracionButton.ConfiguracionButton.posicionY);//configuración
 		 new WebDriverWait(Variables.driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@text='Configuración']"))); // esperamos hasta la pantalla de configuración
 		 tap(Variables.configuracionOptions.vendedoresButton.posicionX,Variables.configuracionOptions.vendedoresButton.posicionY);//Vendedores
 		 new WebDriverWait(Variables.driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@resource-id='icg.android.start:id/summary']"))); // pantalla vendedores mantenimiento.
 		 tap(Variables.configuracionOptions.nuevoVendedorButton.posicionX,Variables.configuracionOptions.nuevoVendedorButton.posicionY);//Nuevo vendedor
 		 new WebDriverWait(Variables.driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@text='VENDEDOR']")));//pantalla de nuevo vendedor
         logger.log(LogStatus.INFO, "Pantalla de nuevo vendedor alcanzada");
         inforarCamposNuevoVendedor();
         logger.log(LogStatus.INFO, "Test 'crear Vendedor' se ha completado con exito <a target='_blank' href='"+Variables.shelper.doScreenShot()+"'>ScreenShot</a>");
         tap(Variables.topBarButtons.CerrarButton.posicionX,Variables.topBarButtons.CerrarButton.posicionY);//cerrar
 		 new WebDriverWait(Variables.driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@resource-id='icg.android.start:id/summary']"))); // pantalla vendedores mantenimiento.
 		 tap(Variables.topBarButtons.CerrarButton.posicionX,Variables.topBarButtons.CerrarButton.posicionY);//cerrar
 		 new WebDriverWait(Variables.driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@text='Configuración']"))); // esperamos hasta la pantalla de configuración
 		 tap(Variables.topBarButtons.CerrarButton.posicionX,Variables.topBarButtons.CerrarButton.posicionY);//cerrar
 		 new WebDriverWait(Variables.driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='roomSurface']")));//pantalla de mesas
 		 tap(Variables.topBarButtons.CerrarButton.posicionX,Variables.topBarButtons.CerrarButton.posicionY);//vendedores entrada/registro
 		 new WebDriverWait(Variables.driver, 120).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='sellerPager']")));//espero hasta detectar pantalla de vendedores. 
	 	 tap(182,44);//close APP
         logger.log(LogStatus.INFO, "App cerrada para refrescar vendedores");
	 	 sleep(1500);
	 	 Variables.driver.startActivity(Variables.HioPosCloudPackage, Variables.HioPosCloudStartActivity);
 		 new WebDriverWait(Variables.driver, 120).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='sellerPager']")));//espero hasta detectar pantalla de vendedores. 
	 	 logger.log(LogStatus.INFO, "App abierta para refrescar vendedores");
	 	 verificarPantallaVenta(true,"Adria HPC");
	 	 validarVendedorCloud();
	 	 LoginVendedor();
	 	 BorrarVendedor();
	 	 verificarPantallaVenta(false,"Adria HPC");
	 	 
		 logger.log(LogStatus.PASS, "Test 'Crear - Login- Borrar Vendedor' se ha completado con exito. <a target='_blank' href='"+Variables.shelper.doScreenShot()+"'>ScreenShot</a>");
 

    }
	 	 

    private void verificarPantallaVenta(boolean visible,String nombreVendedor) {
		// TODO Auto-generated method stub
    	String imagePath=Variables.shelper.doScreenShot();
    	sleep(1500);
    	try {
			ImageResizer.resize(imagePath, "C:\\temp\\test.jpg",960,540);
			sleep(1000);
			Process p = Runtime.getRuntime().exec("C:\\Capture2Text\\Capture2Text.exe -i C:\\temp\\test.jpg -o C:\\temp\\test.txt -l Spanish");
			sleep(20000);
			Boolean vendedorEncontrado = FileUtils.readFileToString((new File("C:\\temp\\test.txt"))).contains(nombreVendedor);
			System.out.println("ENCONTRADO "+vendedorEncontrado+" LO TENIA QUE ENCONTRAR? "+visible);
			
			if (!vendedorEncontrado && !visible) {
		         logger.log(LogStatus.INFO, "El vendedor Adria HPC ya no esta en la pantalla de venta. <a target='_blank' href='"+Variables.shelper.doScreenShot()+"'>ScreenShot</a>");	
		         String resultado =SQLHelper.executeSQL("select count(*) as RESULTADO from DB28445.Con__Seller where ContactId=(select MAX(CONTACTid) from DB28445.Con__Contact where Name='Adria HPC') and IsDiscontinued=1");
					if (resultado.equals("1")) {
						logger.log(LogStatus.INFO, "El vendedor ADRIA HPC se encuentra en la base de datos de CLOUD como discontinued 1.");
						}else {
							logger.log(LogStatus.FAIL, "El vendedor ADRIA HPC no se encuentra en la base de datos de CLOUD como discontinued 1.");
						}
			}
			else if (vendedorEncontrado && visible){
				logger.log(LogStatus.INFO, "El vendedor Adria HPC esta en la pantalla de venta. <a target='_blank' href='"+Variables.shelper.doScreenShot()+"'>ScreenShot</a>");	
				String resultado =SQLHelper.executeSQL("select count(*) as RESULTADO\r\n" + 
		    			"from DB28445.Con__Contact cc,DB28445.Con__Seller cs where cc.ContactId = cs.ContactId and cs.IsDiscontinued=0 and cc.Name='Adria HPC'");
				if (resultado.equals("1")) {
					logger.log(LogStatus.INFO, "El vendedor ADRIA HPC se encuentra en la base de datos de CLOUD como discontinued 0.");
					}else {
						logger.log(LogStatus.FAIL, "El vendedor ADRIA HPC no se encuentra en la base de datos de CLOUD como discontinued 0.");
					}
				
			}else {
		         logger.log(LogStatus.FAIL, "Test 'Crear - Login- Borrar Vendedor' se ha completado, el vendedor Adria HPC sigue estando en la pantalla de venta. <a target='_blank' href='"+Variables.shelper.doScreenShot()+"'>ScreenShot</a>");
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	File imagen = new File("C:\\temp\\test.jpg");
    	imagen.delete();
    	File texto = new File("C:\\temp\\test.txt");
    	texto.delete();
    	
    	
	}

	public void LoginVendedor() {
		 new WebDriverWait(Variables.driver, 120).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='sellerPager']")));//espero hasta detectar pantalla de vendedores.
		 Variables.driver.getKeyboard().sendKeys("2020"+"\n");//login con tarjeta
 		 new WebDriverWait(Variables.driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='roomSurface']")));//pantalla de mesas
	 	 logger.log(LogStatus.INFO, "Login de vendedor Adria HPC con tarjeta se ha realizado con exito.");
 		 tap(Variables.topBarButtons.CerrarButton.posicionX,Variables.topBarButtons.CerrarButton.posicionY);//vendedores entrada/registro
		 new WebDriverWait(Variables.driver, 120).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='sellerPager']")));//espero hasta detectar pantalla de vendedores.
		
		 //Hacer login con password de entrada también.
		 
		 tap(Variables.tecladoNumerico.Dos.posicionX,Variables.tecladoNumerico.Dos.posicionY);
		 tap(Variables.tecladoNumerico.Cero.posicionX,Variables.tecladoNumerico.Cero.posicionY);
		 tap(Variables.tecladoNumerico.Uno.posicionX,Variables.tecladoNumerico.Uno.posicionY);
		 tap(Variables.tecladoNumerico.Nueve.posicionX,Variables.tecladoNumerico.Nueve.posicionY);
		 tap(Variables.tecladoNumerico.Intro.posicionX,Variables.tecladoNumerico.Intro.posicionY);
		 
 		 new WebDriverWait(Variables.driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='roomSurface']")));//pantalla de mesas
	 	 logger.log(LogStatus.INFO, "Login de vendedor Adria HPC con contrasena de acceso se ha realizado con exito.");
 		 tap(Variables.topBarButtons.CerrarButton.posicionX,Variables.topBarButtons.CerrarButton.posicionY);//vendedores entrada/registro
		 new WebDriverWait(Variables.driver, 120).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='sellerPager']")));//espero hasta detectar pantalla de vendedores.
		 

    }
    

    public void BorrarVendedor() {
		 new WebDriverWait(Variables.driver, 120).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='sellerPager']")));//espero hasta detectar pantalla de vendedores.
		 tap(470,291);//login con seller1
		 new WebDriverWait(Variables.driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='roomSurface']")));//pantalla de mesas
		 tap(Variables.topBarButtons.AdministracionButton.posicionX,Variables.topBarButtons.AdministracionButton.posicionY);//administración
 		 tap(Variables.topBarButtons.AdministracionButton.ConfiguracionButton.posicionX,Variables.topBarButtons.AdministracionButton.ConfiguracionButton.posicionY);//configuración
 		 new WebDriverWait(Variables.driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@text='Configuración']"))); // esperamos hasta la pantalla de configuración
 		 tap(Variables.configuracionOptions.vendedoresButton.posicionX,Variables.configuracionOptions.vendedoresButton.posicionY);//Vendedores
 		 new WebDriverWait(Variables.driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@resource-id='icg.android.start:id/summary']"))); // pantalla vendedores mantenimiento.
 		 tap(803,142);
 		 new WebDriverWait(Variables.driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@text='VENDEDOR']")));//pantalla de edición de vendedor.
 		 tap(161,950);//borrar
 		 sleep(1000);
 		 tap(1081,681);//borrar-si
	 	 logger.log(LogStatus.INFO, "Vendedor Borrado");
 		 new WebDriverWait(Variables.driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@resource-id='icg.android.start:id/summary']"))); // pantalla vendedores mantenimiento.
 		 tap(Variables.topBarButtons.CerrarButton.posicionX,Variables.topBarButtons.CerrarButton.posicionY);//cerrar
 		 new WebDriverWait(Variables.driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@text='Configuración']"))); // esperamos hasta la pantalla de configuración
 		 tap(Variables.topBarButtons.CerrarButton.posicionX,Variables.topBarButtons.CerrarButton.posicionY);//cerrar
 		 new WebDriverWait(Variables.driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='roomSurface']")));//pantalla de mesas
 		 tap(Variables.topBarButtons.CerrarButton.posicionX,Variables.topBarButtons.CerrarButton.posicionY);//vendedores entrada/registro
 		 new WebDriverWait(Variables.driver, 120).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='sellerPager']")));//espero hasta detectar pantalla de vendedores
 		 tap(182,44);//close APP
         logger.log(LogStatus.INFO, "App cerrada para refrescar vendedores");
	 	 sleep(1500);
	 	 Variables.driver.startActivity(Variables.HioPosCloudPackage, Variables.HioPosCloudStartActivity);
 		 new WebDriverWait(Variables.driver, 120).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='sellerPager']")));//espero hasta detectar pantalla de vendedores. 
	 	 logger.log(LogStatus.INFO, "App abierta para refrescar vendedores");

    }
    
    private boolean validarVendedorCloud() {
    	
    	String resultado = SQLHelper.executeSQL("select COUNT(*) AS RESULT  from DB28445.Con__Contact cc,DB28445.Con__Seller cs where cc.ContactId = cs.ContactId"
    			+ " and cs.IsDiscontinued=0 and cc.Name='Adria HPC' and cc.FiscalId='123456789G' and \r\n" + 
    			"cc.GenderId=1 and cc.email='dptocalidadicg@gmail.com' and cc.Phone='660580614' and cc.Address='Carrer Mestral 1'\r\n" + 
    			"and cc.PostalCode='25123' and cc.City='Torrefarera' \r\n" + 
    			"");
    	
    	if (resultado.equals("1")) {
   	 	 logger.log(LogStatus.INFO, "Informacion del vendedor Adria HPC validada en el CLOUD correctamente.");

    		return true;
    	}else
    		 logger.log(LogStatus.FAIL, "Información del vendedor Adria HPC NO validada en el CLOUD.");
    	return false;
    }


	private void inforarCamposNuevoVendedor() {
		
		tap(1160,240);//tarjeta
		tap(930,572);//registrar
		Variables.driver.getKeyboard().sendKeys("2020" +"\n");
		tap(896,240);//acceso
		
		tap(483,397);//tap en género.
		tap(973,325);//tap en hombre.
		tap(968,672);//tap en perfil.
		tap(885,482);//tap en administrador.
		
		Variables.driver.findElement(By.xpath("//*[@class='android.widget.EditText' and (./preceding-sibling::* | ./following-sibling::*)[@text='Nombre']]")).sendKeys("Adria HPC");
		Variables.driver.findElement(By.xpath("//*[@class='android.widget.EditText' and (./preceding-sibling::* | ./following-sibling::*)[@text='NIF']]")).sendKeys("123456789G");
		Variables.driver.findElement(By.xpath("//*[@class='android.widget.EditText' and (./preceding-sibling::* | ./following-sibling::*)[@text='Dirección']]")).sendKeys("Carrer Mestral 1");
		Variables.driver.findElement(By.xpath("//*[@class='android.widget.EditText' and (./preceding-sibling::* | ./following-sibling::*)[@text='Código postal']]")).sendKeys("25123");
		Variables.driver.findElement(By.xpath("//*[@class='android.widget.EditText' and (./preceding-sibling::* | ./following-sibling::*)[@text='Población']]")).sendKeys("Torrefarera");
		Variables.driver.findElement(By.xpath("//*[@class='android.widget.EditText' and (./preceding-sibling::* | ./following-sibling::*)[@text='Teléfono']]")).sendKeys("660580614");
		Variables.driver.findElement(By.xpath("//*[@class='android.widget.EditText' and (./preceding-sibling::* | ./following-sibling::*)[@text='Email']]")).sendKeys("dptocalidadicg@gmail.com");
		
		tap(940,403);//tap en contraseña del programa
		tap(1634,887);tap(1593,977);tap(1532,884);tap(1746,684); //2019
		tap(1796,540);//aceptar
		tap(947,510);//tap en contraseña de analytics
		Variables.driver.getKeyboard().sendKeys(".1234abcd");
		tap(1840,32);//aceptar
		tap(1591,944);//tap en guardar.
		logger.log(LogStatus.INFO,"Vendedor <b>Adria HPC</b> guardado - Pass: <b>2019</b> - Tarjeta:  <b>2020</b> - Analytics:<b>.1234abcd</b>");
	}
	
	
	
}
