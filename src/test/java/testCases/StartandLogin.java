package testCases;
//package <set your test package>;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import utils.ScreenShotHelper;
import utils.SimpleOnFailed;
import utils.Variables;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.ScreenOrientation;
import java.net.URL;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;

public class StartandLogin {

    

	static ExtentTest logger;

		
	@Rule
    public SimpleOnFailed ruleExample = new SimpleOnFailed(Variables.driver,logger,Variables.extent,false);

    
    @BeforeClass
    public static void setUp() throws MalformedURLException {
    	
    	try {
			Variables.deleteReports();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	Set<String> artifactoryLoggers = new HashSet<>(Arrays.asList("org.apache.http", "groovyx.net.http"));
		for(String log:artifactoryLoggers) {
		    ch.qos.logback.classic.Logger artLogger = (ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory.getLogger(log);
		    artLogger.setLevel(ch.qos.logback.classic.Level.INFO);
		    artLogger.setAdditive(false);
		}
		
        Variables.dc = Variables.setDesiredCababilities("Arranca y Login",true,true);
        Variables.driver = new AndroidDriver<>(new URL(Variables.URL),Variables.dc);
        Variables.driver.setLogLevel(Level.INFO);
        //driver.rotate(ScreenOrientation.LANDSCAPE);
		Variables.configureReport();	
		Variables.shelper = new ScreenShotHelper(Variables.driver);
		
    }

    @Test(timeout=30000)   
    public void a() {
    	//-------------------------------//
    		logger = Variables.extent.startTest("Arranca y Login");
    		ruleExample.setLogger(logger);
    		ruleExample.browser=Variables.driver;
    		ruleExample.setReport(Variables.extent);
    		
    	//-------------------------------//
    		Variables.driver.startActivity("icg.android.start", ".StartActivity");
    		new WebDriverWait(Variables.driver, 60).until(ExpectedConditions.presenceOfElementLocated(By.xpath(Variables.ViewsTestLogin.pantallaIdiomas)));
    		sleep(1500);
    		logger.log(LogStatus.INFO, "APP iniciada");
    		//driver.rotate(ScreenOrientation.LANDSCAPE);
    		Variables.driver.findElement(By.xpath(Variables.ViewsTestLogin.botonEspanol)).click();
    		new WebDriverWait(Variables.driver, 60).until(ExpectedConditions.presenceOfElementLocated(By.xpath(Variables.ViewsTestLogin.pantallaLogin)));
    		sleep(500);
    		Variables.driver.findElement(By.xpath(Variables.ViewsTestLogin.campoNombreLogin)).sendKeys(Variables.loginName); //Introduciendo nombre de usuario en pantalla login
    		sleep(100);
    		Variables.driver.findElement(By.xpath(Variables.ViewsTestLogin.campoPasswordLogin)).sendKeys(Variables.loginPass); // Introduciendo password usuario en pantalla login
    		tapNextH();
    		sleep(2000);
    		logger.log(LogStatus.INFO, "Hacemos LOGIN con "+Variables.loginName+" / "+Variables.loginPass);
    		Variables.driver.tap(1,1097,676,100); // CONTROLAMOS UNA POSIBLE SUSTITUCI�N DEL TERMINAL.
    		new WebDriverWait(Variables.driver, 20).until(ExpectedConditions.presenceOfElementLocated(By.xpath(Variables.ViewsTestLogin.campoEmailConfiguracionTienda))); // Campo email en configuraci�n
    		MobileElement numeroTiendaElement = (MobileElement) Variables.driver.findElementByXPath(Variables.ViewsTestLogin.campoNumeroTienda);
    		MobileElement emailElement = (MobileElement) Variables.driver.findElementByXPath(Variables.ViewsTestLogin.campoEmailTienda);
    		logger.log(LogStatus.INFO, "Estamos en la pantalla de configuracion de TIENDA "+numeroTiendaElement.getText()+" y E-MAIL: <b>"+emailElement.getText()+"</b>");
    		logger.log(LogStatus.PASS, "Test 'Arranca y Login' se ha completado con exito <a target='_blank' href='"+Variables.shelper.doScreenShot()+"'>ScreenShot</a>");
    		Assert.assertTrue(true);		
	
    }
    
    @Test(timeout=360000)   
    public void b() {
    	//-------------------------------//
    	logger = Variables.extent.startTest("Configurar");
    	ruleExample.setLogger(logger);
		ruleExample.browser=Variables.driver;
		ruleExample.setReport(Variables.extent);
		//-------------------------------//
    	//Leemos todos los campos que se muestran en pantalla.
    	MobileElement campoNombre=(MobileElement)Variables.driver.findElementByXPath(Variables.ViewsConfiguracion.campoNombreConfiguracion);
    	MobileElement campoNombreFiscal=(MobileElement)Variables.driver.findElementByXPath(Variables.ViewsConfiguracion.campoNombreFiscalConfiguracion);
    	MobileElement campoNIF=(MobileElement)Variables.driver.findElementByXPath(Variables.ViewsConfiguracion.campoNIFConfiguracion);
    	MobileElement campoDireccion=(MobileElement)Variables.driver.findElementByXPath(Variables.ViewsConfiguracion.campoDireccionConfiguracion);
    	MobileElement campoCP=(MobileElement)Variables.driver.findElementByXPath(Variables.ViewsConfiguracion.campoCPConfiguracion);
    	MobileElement campoPoblacion=(MobileElement)Variables.driver.findElementByXPath(Variables.ViewsConfiguracion.campoPoblacionConfiguracion);
    	MobileElement campoProvincia=(MobileElement)Variables.driver.findElementByXPath(Variables.ViewsConfiguracion.campoProvinciaConfiguracion);
    	MobileElement campoTelefono=(MobileElement)Variables.driver.findElementByXPath(Variables.ViewsConfiguracion.campoTlfConfiguracion);
    	MobileElement campoEmail=(MobileElement)Variables.driver.findElementByXPath(Variables.ViewsConfiguracion.campoEmailConfiguracion);
    	
    	logger.log(LogStatus.INFO,"Campos encontrados: <br>"+ campoNombre.getText()+" <br> "+
    	campoNombreFiscal.getText()+" <br> "+campoNIF.getText()+" <br> "+campoDireccion.getText()+campoCP.getText()+" <br> "+
    			campoPoblacion.getText()+" <br> "+campoProvincia.getText()+" <br> "+campoTelefono.getText()+" <br> "+campoEmail.getText());

    	tapNextH(); //Avanzamos a la pantalla de configuraci�n de dispositivos. Hay que esperar a que descargue datos. Esperamos 180 segundos max.
    	logger.log(LogStatus.INFO,"Esperando a que descargue datos");
    	new WebDriverWait(Variables.driver, 180).until(ExpectedConditions.presenceOfElementLocated(By.xpath(Variables.ViewsConfiguracion.pantallaDispositivos)));
    	//Estamos en la pantalla de configuraci�n de dispositivos.
    	logger.log(LogStatus.INFO,"Pantalla de dispositivos abierta");
    	sleep(500);
    	Variables.driver.tap(1,376,208,100); //Pulsamos en Modelo de impresora
    	new WebDriverWait(Variables.driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@text='MODELO DE IMPRESORA']")));
    	Variables.driver.findElement(By.xpath(Variables.ViewsConfiguracion.campoModeloImpresora)).click();
    	Variables.driver.tap(1,735,212,100); //Pulsamos en conexi�n de la impresora
    	Variables.driver.findElement(By.xpath(Variables.ViewsConfiguracion.campoConexionImpresora)).click();
    	Variables.driver.tap(1,921,212,100); //Pulsamos en la ip de la impresora.
    	new WebDriverWait(Variables.driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@text='Direcci�n IP']")));
    	Variables.driver.findElement(By.xpath("//*[@class='android.widget.EditText']")).sendKeys("192.168.7.70");
    	Variables.driver.executeScript("seetest:client.deviceAction(\"Enter\")");
    	sleep(500);
    	Variables.driver.tap(1,1269,202,100); //imprimimos prueba
    	logger.log(LogStatus.INFO,"Impresora configurada, test de impresion enviado");
    	tapNextH();
    	new WebDriverWait(Variables.driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@text='IMPRESORAS COCINA']")));
    	logger.log(LogStatus.INFO,"Pantalla de Impresoras de cocina abierta");
    	sleep(500);
    	tapNextH();//Avanzamos ahora con la pantalla en horizontal	
    	new WebDriverWait(Variables.driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='productViewer']")));
    	logger.log(LogStatus.INFO,"Pantalla de Familias de productos abierta");
    	sleep(500);
    	tapNextH();
        new WebDriverWait(Variables.driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@text='Felicidades']")));
        logger.log(LogStatus.INFO, "Pantalla de FELICIDADES");
        sleep(500);
      
       // driver.findElement(By.xpath("//*[@class='android.view.View' and (./preceding-sibling::* | ./following-sibling::*)[@text]]")).click();
        Variables.driver.tap(1, 1316, 531, 100);   //clic iniciar

//        //Hay que verificar si se est� actualizando la app.
    	sleep(2000);//       
         Boolean isPresent = Variables.driver.findElements(By.xpath("//*[@text='Descargando nueva versi�n']")).size() > 0; // verifico si tengo que instalar nueva versi�n.
        
         if(isPresent) {
        	 logger.log(LogStatus.INFO, "Se ha detectado una nueva versi�n para descargar.");
        	 new WebDriverWait(Variables.driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='ok_button']")));
        	 Variables.driver.findElement(By.xpath("//*[@id='ok_button']")).click();
            
        	 esperarInstalacion();
        	 
             Variables.driver.startActivity("icg.android.start", ".StartActivity");
             logger.log(LogStatus.INFO, "Abriendo la App");
             
         }else {
        	 logger.log(LogStatus.SKIP, "No hay actualizaciones pendientes de descargar");
         }

        new WebDriverWait(Variables.driver, 120).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='sellerPager']")));
        logger.log(LogStatus.INFO, "Pantalla de Vendedores - HioPosCloud Configurado");
    	logger.log(LogStatus.PASS, "Test 'Configurar' se ha completado con exito <a target='_blank' href='"+Variables.shelper.doScreenShot()+"'>ScreenShot</a>");
        Assert.assertTrue(true);
    }
    
    private void esperarInstalacion() {
		// TODO Auto-generated method stub
    	//*[@text='Installing�']
    	Boolean isPresent = Variables.driver.findElements(By.xpath("//*[@text='Installing�']")).size() > 0; // verifico si tengo que instalar nueva versi�n.
    	while (isPresent) {
    		 isPresent = Variables.driver.findElements(By.xpath("//*[@text='Installing�']")).size() > 0; // verifico si tengo que instalar nueva versi�n.
    		 sleep(10000);
    	}

	}

	@After 
    public void afterEachTest() {
    	  //logger.log(LogStatus.INFO,"<a target='_blank' href='"+shelper.doScreenShot()+"'>ScreenShot</a>");
    	Variables.extent.endTest(logger);
    	Variables.extent.flush();
    }
  
    @AfterClass
    public static void  tearDown() {
    	 // extent.close();
    	  sleep(2000);
    	//driver.removeApp("icg.android.start");
    	//logger.log(LogStatus.INFO, "APP desinstalada "+Variables.HioPosCloudPackage);
       // driver.quit();
    }
    
    /*
     * Avanzar a la siguiente pantalla de configuraci�n durante el inicio de la app.
     */
    public void tapNext() {
    	Variables.driver.tap(1, 718, 32, 100);   
    }
    
    /*
     * Avanzar a la siguiente pantalla de configuraci�n durante el inicio de la app.(PANTALLA HORIZONTAL)
     */
    public void tapNextH() {
    	Variables.driver.tap(1,1804, 44, 100);   
    }
    
    public static void sleep(int mili) {
    	 try{Thread.sleep(mili);} catch(Exception ignore){}
    }
   
    
    
    
}