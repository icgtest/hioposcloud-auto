package testSuite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import testCases.RegistrarVendedor;
import testCases.StartandLogin;
import testCases.Vendedores;

@RunWith(Suite.class)
@Suite.SuiteClasses(
		{
			StartandLogin.class,
			RegistrarVendedor.class,
			Vendedores.class
			
		})


public class testSuite {
	
	 static {
	      System.setProperty("org.apache.commons.logging.Log",
	                         "org.apache.commons.logging.impl.NoOpLog");
	   }

}
